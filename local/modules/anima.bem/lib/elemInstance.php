<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 21.04.15
 * Time: 13:43
 */

namespace Anima\Bem;


class ElemInstance extends BlockInstance {
    var $elemName;

    function __construct($__blockName, $__elemName)
    {
        $this->blockName = strtolower($__blockName);
        $this->elemName = strtolower($__elemName);

        $blockPath = $_SERVER['DOCUMENT_ROOT'].'/local/blocks/'.$this->blockName.'/__'.$this->elemName;

        if(is_dir($blockPath))
        {
            $this->fullPath = $blockPath;
            $this->path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $blockPath.'/');
        }

        $this->getBlockDeps();
        $this->findBlockFiles();
        $this->findMods();

        return $this;
    }
}