'use strict';

var PATH = require('path'),
    environ = require('bem-environ'),
    SASS = require('node-sass'),
    BEMCORE_TECHS = PATH.resolve(environ.PRJ_ROOT, 'node_modules/bem/lib/techs/v2');

exports.baseTechPath = require.resolve(BEMCORE_TECHS+'/css-preprocessor.js');

exports.techMixin = {

    template: ['{{bemSelector}}', "\t"],

    getBuildSuffixesMap: function() {
        return {
            'sass.css': ['sass', 'css']
        };
    },

    getCreateSuffixes: function() {
        return ['sass'];
    },

    compileBuildResult: function(res, defer) {

        SASS.render({
            data: res,
            success: function(css) {
                return defer.resolve([css]);
            },
            error: function(err) {
                return defer.reject(err);
            }
        });

    }

};
