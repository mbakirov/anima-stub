var PATH = require('path'),
    environ = require('bem-environ'),
    getTechResolver = environ.getTechResolver;

environ.PRJ_ROOT = PATH.resolve('./');

var PRJ_ROOT = environ.PRJ_ROOT,
    PRJ_TECHS = PATH.resolve(PRJ_ROOT, 'local/.bem/techs/v2'),
    //BEMCORE_TECHS = environ.getLibPath('bem-core', '.bem/techs');
    BEMCORE_TECHS = PATH.resolve(environ.PRJ_ROOT, 'node_modules/bem/lib/techs/v2');

exports.getTechs = function() {
    var techs = {
        'sass'          : 'v2/sass',
        'scss'          : 'v2/scss',
        'css'           : 'v2/css',
        'js'            : 'v2/js-i',
        'deps.js'       : 'v2/deps.js'
    };

    // use techs from project (.bem/techs)
    //['sass', 'scss', 'bemhtml', 'bemjson.js'].forEach(getTechResolver(techs, PRJ_TECHS));
    ['sass', 'scss', 'bemjson.js', 'browser.js', 'vanilla.js', 'node.js',  'bemtree'].forEach(getTechResolver(techs, PRJ_TECHS));

    // use techs from bem-core library
    ['deps.js', 'js'].forEach(getTechResolver(techs, BEMCORE_TECHS));

    return techs;
};

exports.defaultTechs = ['js', 'deps.js', 'sass'];
